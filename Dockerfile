# Liferay Portal 6.2 EE Compatible Environment
# https://www.liferay.com/documents/14/21598941/Liferay+Portal+6.2+EE+Compatibility+Matrix.pdf

# CentOS 6 (6-6.el6.centos.12.2.x86_64)
FROM		centos:6

# Oracle JDK 7 (7u80-b15)
RUN		yum install -y wget \
	&&	wget 	--no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" \
			http://download.oracle.com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-x64.rpm \
	&&	rpm -Uvh jre-7u80-linux-x64.rpm \
	&&	rm -f jre-7u80-linux-x64.rpm

ENV		JAVA_OPTS				-Xms1024m -Xmx1024m -XX:MaxPermSize=256m


